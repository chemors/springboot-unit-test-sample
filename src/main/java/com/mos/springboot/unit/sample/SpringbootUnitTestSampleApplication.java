package com.mos.springboot.unit.sample;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot单元测试示例应用程序
 *
 * @author 小尘哥
 * @date 2022/10/13
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.mos.springboot.unit.sample.mapper"})
public class SpringbootUnitTestSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUnitTestSampleApplication.class, args);
    }
}
