package com.mos.springboot.unit.sample.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mos.springboot.unit.sample.entity.Student;
import com.mos.springboot.unit.sample.mapper.StudentMapper;
import com.mos.springboot.unit.sample.service.IStudentService;
import org.springframework.stereotype.Service;

/**
 * 学生服务
 *
 * @author 小尘哥
 * @date 2022/10/13
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


    @Override
    public Student getByCardNum(Integer cardNum) {
        return createMock(cardNum);
    }

    private Student createMock(Integer cardNum) {
        return Student.builder().id(StrUtil.uuid()).name("张三").cardNum(cardNum).build();
    }
}
