package com.mos.springboot.unit.sample.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mos.springboot.unit.sample.entity.Student;

/**
 * student服务类
 *
 * @author 小尘哥
 * @date 2022/10/13
 */
public interface IStudentService extends IService<Student> {

    /**
     * 根据校园卡卡号获取学生信息
     *
     * @param cardNum 卡num
     * @return {@link Student}
     */
    Student getByCardNum(Integer cardNum);
}
