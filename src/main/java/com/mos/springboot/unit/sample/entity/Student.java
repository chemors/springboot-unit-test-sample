package com.mos.springboot.unit.sample.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * 学生
 *
 * @author 小尘哥
 * @date 2022/10/13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@TableName("t_student")
public class Student implements Serializable {

    /**
     * id
     */
    private String id;

    /**
     * 名字
     */
    private String name;

    /**
     * 性别
     */
    private String sex;

    /**
     * 校园卡卡号
     */
    private Integer cardNum;
}
