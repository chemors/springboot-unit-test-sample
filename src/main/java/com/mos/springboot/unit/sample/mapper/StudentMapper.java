package com.mos.springboot.unit.sample.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mos.springboot.unit.sample.entity.Student;

/**
 * 学生映射器
 *
 * @author 小尘哥
 * @date 2022/10/18
 */
public interface StudentMapper extends BaseMapper<Student> {
}
