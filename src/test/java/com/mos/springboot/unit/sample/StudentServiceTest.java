package com.mos.springboot.unit.sample;

import cn.hutool.core.util.RandomUtil;
import com.mos.springboot.unit.sample.entity.Student;
import com.mos.springboot.unit.sample.service.IStudentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@DisplayName("学生服务测试类")
@SpringBootTest
public class StudentServiceTest {

    @Resource
    private IStudentService studentService;

    @DisplayName("根据校园卡号获取学生信息(有效卡号)")
    @Test
     void validCardNum(){
        Integer cardNum = 9999;
        Student student = studentService.getByCardNum(cardNum);
        Assertions.assertNotNull(student ,"student must not be null");
    }

    @DisplayName("根据校园卡号获取学生信息(无效卡号)")
    @Test
    void unValidCardNum(){
        try {
            Student student = studentService.getByCardNum(Integer.parseInt("abc"));
        }catch (Exception e){
            Assertions.assertNotNull(e instanceof NumberFormatException ,"卡号转换失败，非number format异常");
        }

    }

    @DisplayName("保存学生信息")
    @Test
    @Transactional
    void save(){
        Student student = studentService.getByCardNum(RandomUtil.randomInt());
        boolean r = studentService.save(student);
        Assertions.assertTrue(r);
    }
}
