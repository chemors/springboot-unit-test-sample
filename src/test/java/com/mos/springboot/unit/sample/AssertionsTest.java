package com.mos.springboot.unit.sample;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@DisplayName("断言测试")
@SpringBootTest
public class AssertionsTest {

    private static int a , b;
    private static Integer[] c, d;


    @BeforeAll
    static void before(){
        a = 1;
        b = 1;
        c = new Integer[]{1, 2, 3};
        d = new Integer[]{1, 4, 3};
    }

    @Test
    void assertionsDemo(){

        boolean result = a == b;
        Assertions.assertTrue(result);
        Assertions.assertNotNull(result,"result must be not null");
        Assertions.assertEquals(a,b);//这里可用在某些持久层框架添加、修改、删除时返回值为int类型的数据处理条数时
        Assertions.assertArrayEquals(c,d,"期望值是:{1,2,3}");
    }
}
